﻿using System;

namespace RPG
{
    class Item
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="slot"></param>
        /// <param name="type"></param>
        /// <param name="minLevel"></param>
        /// <param name="damage"></param>
        /// <param name="attackSpeed"></param>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public Item(string name, string slot, string type, short minLevel, long damage, double attackSpeed, long strength, long dexterity, long intelligence)
        {
            Name = name;
            if (slot == "head" || slot == "body" || slot == "pants" || slot == "weapon") Slot = slot;
            else
            {
                Console.WriteLine("That slot does not exist!");
                ValidItem = false;
            }
            if (type == "axe" || type == "bow" || type == "dagger" || type == "hammer" || type == "staff" || type == "sword" || type == "wand" ||
                type == "cloth" || type == "leather" || type == "mail" || type == "plate") Type = type;
            else
            {
                Console.WriteLine("Wrong item type!");
                ValidItem = false;
            }
            if (minLevel >= 1 && minLevel <= 10) MinLevel = minLevel;
            else
            {
                Console.WriteLine("Wrong item level!");
                ValidItem = false;
            }
            if (Slot == "weapon")
            {
                if (damage >= 1 && attackSpeed >= 1)
                {
                    Damage = damage;
                    AttackSpeed = attackSpeed;
                }
                else
                {
                    Console.WriteLine("Damage and attack speed must be 1 or greater!");
                    ValidItem = false;
                }
            }
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public string Name { get; set; } = "";
        public string Slot { get; set; } = "";
        public string Type { get; set; } = "";
        public short MinLevel { get; set; } = 1;
        public long Strength { get; set; } = 0;
        public long Dexterity { get; set; } = 0;
        public long Intelligence { get; set; } = 0;
        public long Damage { get; set; } = 0;
        public double AttackSpeed { get; set; } = 0;
        public bool ValidItem { get; set; } = true;
    }
}
