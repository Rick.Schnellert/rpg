using RPG;
using System;
using Xunit;

namespace RPG_TEST
{
    public class UnitTest1
    {
        [Fact]
        //Test 1
        public void TestHighLevelWeapon_InvalidWeaponException()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testAxe = new Item("Test Axe", "weapon", "axe", 2, 1, 1, 0, 0, 0);

            //Act
            testWarrior.Weapon = testAxe;

            //Expected
            if (testWarrior.Weapon == null) throw new InvalidWeaponException();
            else Console.WriteLine("TestHighLevelWeapon: Test should have Failed!");
        }
        [Fact]
        //Test 2
        public void TestHighLevelArmor_InvalidArmorException()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testArmor = new Item("Test Plate", "body", "plate", 2, 1, 1, 0, 0, 0);

            //Act
            testWarrior.Body = testArmor;

            //Expected
            if (testWarrior.Body == null) throw new InvalidArmorException();
            else Console.WriteLine("TestHighLevelArmor: Test should have Failed!");
        }
        [Fact]
        //Test 3
        public void TestWrongWeapon_InvalidWeaponException()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testBow = new Item("Test Bow", "weapon", "bow", 1, 1, 1, 0, 0, 0);

            //Act
            testWarrior.Weapon = testBow;

            //Expected
            if (testWarrior.Weapon == null) throw new InvalidWeaponException();
            else Console.WriteLine("TestWrongWeapon: Test should have Failed!");
        }
        [Fact]
        //Test 4
        public void TestWrongArmor_InvalidArmorException()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testArmor = new Item("Test Cloth", "body", "cloth", 1, 1, 1, 0, 0, 0);

            //Act
            testWarrior.Body = testArmor;

            //Expected
            if (testWarrior.Body == null) throw new InvalidArmorException();
            else Console.WriteLine("TestWrongArmor: Test should have Failed!");
        }
        [Fact]
        //Test 5
        public void TestEquipWeapon_NewWeaponEquiped()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testAxe = new Item("Test Axe", "weapon", "axe", 1, 1, 1, 0, 0, 0);

            //Act
            testWarrior.Weapon = testAxe;

            //Expected
            if (testWarrior.Weapon == testAxe) Console.WriteLine("TestEquipWeapon: Test Success");
            else throw new InvalidWeaponException();
        }
        [Fact]
        //Test 6
        public void TestEquipArmor_NewArmorEquipped()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testArmor = new Item("Test Plate", "body", "plate", 1, 1, 1, 0, 0, 0);

            //Act
            testWarrior.Body = testArmor;

            //Expected
            if (testWarrior.Body == testArmor) Console.WriteLine("TestEquipArmor: Test Success");
            else throw new InvalidArmorException();
        }
        [Fact]
        //Test 7
        public void TestDamageWithoutWeapon_ExpectedDamage()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");

            //Act
            double testDamage = testWarrior.Damage;

            //Expected
            if (testDamage == 1 * (1 + (5 / 100))) Console.WriteLine("TestDamageWithoutWeapon: Test Success");
            else throw new InvalidDamageException();
        }
        [Fact]
        //Test 8
        public void TestDamageWithWeapon_ExpectedDamage()
        {
            //Assert
            Warrior testWarrior = new Warrior("Karl");
            Item testAxe = new Item("Test Axe", "weapon", "axe", 1, 7, 1.1, 0, 0, 0);
            testWarrior.Weapon = testAxe;

            //Act
            double testDamage = testWarrior.Damage;
            //Expected
            if (testDamage == (7 * 1.1) * (1.0 + (5 / 100.0))) Console.WriteLine("TestDamageWithWeapon: Test Success");
            else throw new InvalidDamageException();
        }
        [Fact]
        //Test 8
        public void TestDamageWithWeaponAndArmor_ExpectedDamage()
        {
            //Assert
            Mage testMage = new Mage("Merlin");
            Item testWand = new Item("Test Wand", "weapon", "wand", 1, 2, 1.5, 0, 0, 0);
            Item testHood = new Item("Test Hood", "head", "cloth", 1, 1, 1, 0, 0, 3);
            Item testRobe = new Item("Test Robe", "body", "cloth", 1, 1, 1, 0, 0, 5);
            Item testPants = new Item("Test Pants", "pants", "cloth", 1, 1, 1, 0, 0, 2);
            testMage.Weapon = testWand;
            testMage.Head = testHood;
            testMage.Body = testRobe;
            testMage.Pants = testPants;

            //Act
            double testDamage = testMage.Damage;

            //Expected
            if (testDamage == (2.0 * 1.5) * (1.0 + (18.0 / 100.0))) Console.WriteLine("TestDamageWithWeaponAndArmor: Test Success");
            else throw new InvalidDamageException();
        }
    }
}
