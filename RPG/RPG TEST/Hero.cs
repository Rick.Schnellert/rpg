﻿using System;

namespace RPG
{
    class Hero 
    {
        long _strength = 1;
        long _dexterity = 1;
        long _intelligence = 1;
        private Item _head = null;
        private Item _body = null;
        private Item _pants = null;
        private Item _weapon = null;
        public Item Head 
        {
            get => _head;
            set
            {
                if ((value == null || Validate(value)) && value.Slot == "head")
                    _head = value;
                else Console.WriteLine("This is not a headarmor!");
            }
        }
        public Item Body
        {
            get => _body;
            set
            {
                if ((value == null || Validate(value)) && value.Slot == "body")
                    _body = value;
                else Console.WriteLine("This is not a bodyarmor!");
            }
        }

        public Item Pants
        {
            get => _pants;
            set
            {
                if ((value == null || Validate(value)) && value.Slot == "pants")
                    _pants = value;
                else Console.WriteLine("These are no pants!");
            }
        }
        public Item Weapon
        {
            get => _weapon;
            set
            {
                if ((value == null || Validate(value)) && value.Slot == "weapon")
                    _weapon = value;
                else Console.WriteLine("This is not a weapon!");
            }
        }
        public string Name { get; set; }
        public long Level { get; set; } = 1;
        public long Strength
        {
            get { return _strength + (_head?.Strength ?? 0) + (_body?.Strength ?? 0) + (_pants?.Strength ?? 0) + (_weapon?.Strength ?? 0); }
            set { _strength = value; }
        }

        public long Dexterity
        {
            get { return _dexterity + (_head?.Dexterity ?? 0) + (_body?.Dexterity ?? 0) + (_pants?.Dexterity ?? 0) + (_weapon?.Dexterity ?? 0); }
            set { _dexterity = value; }
        }

        public long Intelligence
        {
            get { return _intelligence + (_head?.Intelligence ?? 0) + (_body?.Intelligence ?? 0) + (_pants?.Intelligence ?? 0) + (_weapon?.Intelligence ?? 0); }
            set { _intelligence = value; }
        }
        public bool PrimeStr { get; set; } = false;
        public bool PrimeDex { get; set; } = false;
        public bool PrimeInt { get; set; } = false;
        public double Damage
        {
            get 
            { 
                if(_weapon != null)
                {
                    if(PrimeInt) return _weapon.Damage * _weapon.AttackSpeed * (1.0 + Intelligence / 100.0);
                    if(PrimeDex) return _weapon.Damage * _weapon.AttackSpeed * (1.0 + Dexterity / 100.0);
                    if(PrimeStr) return _weapon.Damage * _weapon.AttackSpeed * (1.0 + Strength / 100.0);
                    return 0;
                }
                else
                {
                    if (PrimeInt) return 1 + Intelligence / 100;
                    if (PrimeDex) return 1 + Dexterity / 100;
                    if (PrimeStr) return 1 + Strength / 100;
                    return 0;
                }
            }
        }
        public string[] AllowedItems { get; set; }
        private bool Validate(Item value)
        {
            if (value.ValidItem)
            {
                if (value.MinLevel <= Level)
                {
                    foreach (string e in AllowedItems)
                    {
                        if (value.Type.Contains(e))
                        {
                            Console.WriteLine($"New {e} equipped");
                            return true;
                        }
                    }
                    Console.WriteLine("Can only equip: ");
                    foreach (string e in AllowedItems)
                    {
                        Console.WriteLine($"{e}");
                    }
                }
                else Console.WriteLine("Item level is too high!");
            }
            else
            {
                Console.WriteLine("No valid item!");
            }
            return false;
        }
    }
    class Mage : Hero
    {
        public Mage(string _name)
        {
            Name = _name;
            Intelligence = 8;
            PrimeInt = true;
            AllowedItems = new string[] { "staff", "wand", "cloth" };
        }

        public void LevelUp()
        {
            Strength++;
            Dexterity++;
            Intelligence += 5;
            Level++;
        }
    }
    class Ranger : Hero
    {
        public Ranger(string _name)
        {
            Name = _name;
            Dexterity = 7;
            PrimeDex = true;
            AllowedItems = new string[] { "bow", "leather", "mail" };
        }

        public void LevelUp()
        {
            if (Level <= 10)
            {
                Strength += 1;
                Dexterity += 5;
                Intelligence += 1;
                Level++;
            }
            else
            {
                Console.WriteLine("The Hero has reached Max Level already!");
            }
        }
    }
    class Rogue : Hero
    {
        public Rogue(string _name)
        {
            Name = _name;
            Strength = 2;
            Dexterity = 6;
            PrimeDex = true;
            AllowedItems = new string[] { "dagger", "sword", "mail", "leather" };
        }
        public void LevelUp()
        {
            if(Level <= 10) 
            { 
                Strength++;
                Dexterity += 4;
                Intelligence++;
                Level++;
            }
            else
            {
                Console.WriteLine("The Hero has reached Max Level already!");
            }
        }
    }
    class Warrior : Hero
    {
        public Warrior(string _name)
        {
            Name = _name;
            Strength = 5;
            Dexterity = 2;
            PrimeStr = true;
            AllowedItems = new string[] { "axe", "hammer", "sword", "mail", "plate" };
        }
        public void LevelUp()
        {
            if (Level <= 10)
            {
                Strength += 3;
                Dexterity += 2;
                Intelligence++;
                Level++;
            }
            else
            {
                Console.WriteLine("The Hero has reached Max Level already!");
            }  
        }
    }
}
