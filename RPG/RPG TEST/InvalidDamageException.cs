﻿using System;
using System.Runtime.Serialization;

namespace RPG_TEST
{
    [Serializable]
    internal class InvalidDamageException : Exception
    {
        public InvalidDamageException()
        {
        }

        public InvalidDamageException(string message) : base(message)
        {
        }
        public override string Message => "Invalid Damage Exception";
        public InvalidDamageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidDamageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}