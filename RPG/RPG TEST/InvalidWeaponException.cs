﻿using System;
using System.Runtime.Serialization;

namespace RPG_TEST
{
    [Serializable]
    internal class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }

        public InvalidWeaponException(string message) : base(message)
        {
        }
        public override string Message => "Invalid Weapon Exception";

        public InvalidWeaponException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidWeaponException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}